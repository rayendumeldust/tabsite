FROM pandoc/ubuntu:2.10

RUN apt-get -q --no-allow-insecure-repositories update \
  && DEBIAN_FRONTEND=noninteractive \
     apt-get install --assume-yes --no-install-recommends \
     python3\
     python3-pip\
  && rm -rf /var/lib/apt/lists/**

RUN pip3 install pandocfilters
