# Tabs

## Interpret: Titel

!Verse C G am F

!Chorus C C G G am am F F

!Chordonly %X/2.7/1.9/3.8/2.X/X.X/X[E?]

!Melody $A 3 3 5 7 | $6 3 3 $5 5 7 | $E 5 5 $A 5 7 | $E 1 1 $A 7 5 ||
