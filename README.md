


## Pandoc Filter

* https://github.com/jgm/pandocfilters/blob/master/examples/comments.py
* [[https://pandoc.org/filters.html]]

```
pandoc  -t native --filter ./python_filter.py test.txt
pandoc  -t html --filter ./python_filter.py test.txt
pandoc  -t html --filter ./python_filter.py tabs.md
pandoc -t html --template template.html --filter ./python_filter.py tabs.md -o dist/tabs.html
```


## Pandoc Container

* User this container: https://hub.docker.com/r/pandoc/ubuntu/tags
* Install: python3 python3-pip
* pip install pandocfilters
* Similar to this: https://github.com/pandoc/dockerfiles/blob/master/ubuntu/Dockerfile

* Make git public, pull with git
* Run script and check output

* Create Dockerfile
* Push to gitlab
* Use internal docker registry to build

Tagging:

docker build -t registry.gitlab.com/rayendumeldust/tabsite .
