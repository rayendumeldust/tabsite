#!/usr/bin/env python3

"""
Pandoc filter to convert all level 2+ headings to paragraphs with
emphasized text.

Run with:
    pandoc  -t native --filter ./python_filter.py test.txt
"""

import re

from pandocfilters import toJSONFilter, Emph, Para, RawInline, RawBlock

def behead(key, value, f, meta):
    if key == 'Para':
        beginning = value[0]
        #raise Exception(line_content)
        regex = re.compile(r"!([A-Za-z]+)")
        match = regex.search(beginning['c'])
        if match:
            line_content = ' '.join([x['c'] for x in value[1:] if 'c' in x])
            tab_class = match.group(1)
            if tab_class == 'Chordonly':
                chordonly_template = '<div class="jtab chordonly"> %X/2.7/1.9/3.8/2.X/X.X/X[E?] </div>'
                return [RawBlock('html', chordonly_template)]
            if tab_class == 'Melody':
                tabonly_template = '<div class="jtab tabonly">{}</div>'
                return [RawBlock('html', tabonly_template.format(line_content))]

            #value[0]['c'] = template
            tab_div = '<div class="chords {0}"><p><b>{1}:</b>{2}</p></div>'
            return [RawBlock('html', tab_div.format(tab_class.lower(), tab_class, line_content))]


if __name__ == "__main__":
    toJSONFilter(behead)
